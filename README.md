# Emulsify Patch
Set of patches to be used with `emulsify` framework:
- responsive image tag with one or multiple `<source>` tags

## Install
Within the custom Drupal theme root directory run:
- OSX/Linux:
```
git clone https://gitlab.com/nikola.ffw/emulsify-patch.git temp && rm -rf temp/.git/ temp/*.*  && rsync -r temp/components/ components/ && rm -rf temp/
```